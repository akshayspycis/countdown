package classmanager.config;

import datamanager.config.Config;
import datamanager.config.ConfigScheme;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;

public class ConfigSchemeMgr {
        
    public boolean insScheme(ConfigScheme[] scheme,String category_id) {
     try{
         File inputFile = new File("src/dist/sections.xml");
        DocumentBuilderFactory dbFactory =DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc1 = dBuilder.parse(inputFile);
        doc1.getDocumentElement().normalize();
        DocumentTraversal traversal = (DocumentTraversal) doc1;
        Node a = doc1.getDocumentElement();
        NodeIterator iterator = traversal.createNodeIterator(a, NodeFilter.SHOW_ELEMENT, null, true);
        Element b = null;
        for (Node n = iterator.nextNode(); n != null; n = iterator.nextNode()) {
            Element e = (Element) n;
                if(e.getAttribute("category_id").equals(category_id)){
                    b = e;
                    a.removeChild(b);
                }
        }
        Element rootElement = doc1.getDocumentElement();
        for (int i = 0; i < scheme.length; i++) {
            Element category_details = doc1.createElement("section_details");
            rootElement.appendChild(category_details);
            Attr attr = doc1.createAttribute("category_id");
            attr.setValue(scheme[i].getCategory_id());
            category_details.setAttributeNode(attr);
            Element amount = doc1.createElement("amount");
            amount.appendChild(doc1.createTextNode(scheme[i].getAmount()));
            category_details.appendChild(amount);
            Element percentage = doc1.createElement("percentage");
            percentage.appendChild(doc1.createTextNode(scheme[i].getPercentage()));
            category_details.appendChild(percentage);
         }
         
         TransformerFactory transformerFactory1 =TransformerFactory.newInstance();
         
         Transformer transformer1 =transformerFactory1.newTransformer();
         DOMSource source = new DOMSource(doc1);
         StreamResult result =new StreamResult(new File("src/dist/section.xml"));
         transformer1.transform(source, result);
         // Output to console for testing
//         StreamResult consoleResult =new StreamResult(System.out);
//         transformer1.transform(source, consoleResult);
         Config.configmgr.loadScheme();
         return true;
      } catch (Exception e) {
         e.printStackTrace();
         JOptionPane.showMessageDialog(null,e.getMessage(),"Error.",JOptionPane.ERROR_MESSAGE);
         return false;
      }
    }
}
    
    

