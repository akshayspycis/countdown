package classmanager.config;

import datamanager.config.Config;
import datamanager.config.ConfigCategory;
import datamanager.config.ConfigColorDetails;
import datamanager.config.ConfigScheme;
import datamanager.config.FontSetting;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import modules.Home;
import modules.configuration.CategoryManagement;
import modules.configuration.ColorDetails;
import modules.configuration.FontDetails;
import modules.configuration.ProductConfiguration;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ConfigMgr {    

    public boolean loadDatabase() {
        try {
//            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
//            String userName = "root";
//            String password = "root";
//            String url = "jdbc:mysql://localhost:3307/newsuperteacentre";
//            Config.conn = DriverManager.getConnection(url, userName, password);
//            Config.stmt = Config.conn.createStatement();            
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean loadCategory() {
        Config.configcategory  = new ArrayList<ConfigCategory>();
         try {
         File inputFile = new File("src/dist/category.xml");
         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
         Document doc = dBuilder.parse(inputFile);
         doc.getDocumentElement().normalize();
         NodeList nList = doc.getElementsByTagName("category_details");
         for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                ConfigCategory s = new ConfigCategory();                
                Element eElement = (Element) nNode;
                s.setCategory_id(eElement.getAttribute("category_id"));     
                s.setCategory(eElement.getFirstChild().getTextContent());     
                Config.configcategory.add(s);
            }
         }
         return true;
      } catch (Exception e) {
          JOptionPane.showMessageDialog(null,e.getMessage(),"Error.",JOptionPane.ERROR_MESSAGE);
         e.printStackTrace();
         return false;
      }
    }
    public boolean loadScheme() {
        Config.configscheme  = new ArrayList<ConfigScheme>();
        try {
         File inputFile = new File("src/dist/section.xml");
         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
         Document doc = dBuilder.parse(inputFile);
         doc.getDocumentElement().normalize();
         NodeList nList = doc.getElementsByTagName("section_details");
         for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                ConfigScheme s = new ConfigScheme();                
                Element eElement = (Element) nNode;
                s.setCategory_id(eElement.getAttribute("category_id"));     
                s.setAmount(eElement.getElementsByTagName("amount").item(0).getTextContent().trim());     
                s.setPercentage(eElement.getElementsByTagName("percentage").item(0).getTextContent().trim());     
                
                Config.configscheme.add(s);
            }
         }
         return true;
      } catch (Exception e) {
          JOptionPane.showMessageDialog(null,e.getMessage(),"Error.",JOptionPane.ERROR_MESSAGE);
         e.printStackTrace();
         return false;
      }
        
    }
    
    public boolean loadSetting() {
        Config.config_colordetails  = new ConfigColorDetails();
        Properties prop = new Properties();
	InputStream input = null;
        try {
            String filename = "src/dist/setting.properties";
		input = new FileInputStream(filename);
		if (input == null) {
			System.out.println("Sorry, unable to find " + filename);
			return false;
		}
		prop.load(input);
		Config.config_colordetails.setSec_txt(prop.getProperty("sec_txt"));
		Config.config_colordetails.setSec_bg(prop.getProperty("sec_bg"));
		Config.config_colordetails.setTim_txt(prop.getProperty("tim_txt"));
		Config.config_colordetails.setTim_bg(prop.getProperty("tim_bg"));
		Config.config_colordetails.setLogo_txt(prop.getProperty("logo_txt"));
		Config.config_colordetails.setLogo_bg(prop.getProperty("logo_bg"));
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean loadFontSetting() {
        Config.fontsetting  = new FontSetting();
        Properties prop = new Properties();
	InputStream input = null;
        try {
            String filename = "src/dist/font.properties";
		input = new FileInputStream(filename);
		if (input == null) {
			System.out.println("Sorry, unable to find " + filename);
			return false;
		}
		prop.load(input);
		Config.fontsetting.setSec_fname(prop.getProperty("sec_fname"));
		Config.fontsetting.setTim_fname(prop.getProperty("tim_fname"));
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
  
    public boolean loadClassManager() {
        try {
            Config.configschememgr = new ConfigSchemeMgr();
            Config.configcategorymgr = new ConfigCategoryMgr();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }    
    
    public boolean loadForms() {
        try {
            
            Config.home = new Home();
            Config.productconfiguration = new ProductConfiguration(null, true);
            Config.categorymanagement = new CategoryManagement(null, true);
            Config.colordetails = new ColorDetails(null, true);
            Config.fontdetails = new FontDetails(null, true);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }        
    }
      
}
