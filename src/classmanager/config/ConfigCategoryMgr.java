/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classmanager.config;

import datamanager.config.Config;
import datamanager.config.ConfigCategory;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.File;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
/**
 *
 * @author Akshay
 */
public class ConfigCategoryMgr {
        
    public boolean insCategory(ConfigCategory[] scheme){
        try{
        DocumentBuilderFactory dbFactory =DocumentBuilderFactory.newInstance();
         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
         Document doc = dBuilder.newDocument();
         // root element
         Element rootElement = doc.createElement("root");
         doc.appendChild(rootElement);
         
        for (int i = 0; i < scheme.length; i++) {
            Element category_details = doc.createElement("category_details");
            rootElement.appendChild(category_details);

         Attr attr = doc.createAttribute("category_id");
         attr.setValue(scheme[i].getCategory_id());
         category_details.setAttributeNode(attr);
         Element category = doc.createElement("category");
         category.appendChild(doc.createTextNode(scheme[i].getCategory()));
         category_details.appendChild(category);
         
         }
         TransformerFactory transformerFactory =TransformerFactory.newInstance();
         Transformer transformer =transformerFactory.newTransformer();
         DOMSource source = new DOMSource(doc);
         StreamResult result =new StreamResult(new File("src/dist/category.xml"));
         transformer.transform(source, result);
         // Output to console for testing
         StreamResult consoleResult =new StreamResult(System.out);
         transformer.transform(source, consoleResult);
         Config.configmgr.loadCategory();
         return true;
      } catch (Exception e) {
         e.printStackTrace();
         return false;
      }
        
    }    
}
