/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager.config;

/**
 *
 * @author Akshay
 */
public class ConfigColorDetails {
String sec_txt="";
String sec_bg="";
String tim_txt="";
String tim_bg="";
String logo_txt="";
String logo_bg="";

    public String getSec_txt() {
        return sec_txt;
    }

    public void setSec_txt(String sec_txt) {
        this.sec_txt = sec_txt;
    }

    public String getSec_bg() {
        return sec_bg;
    }

    public void setSec_bg(String sec_bg) {
        this.sec_bg = sec_bg;
    }

    public String getTim_txt() {
        return tim_txt;
    }

    public void setTim_txt(String tim_txt) {
        this.tim_txt = tim_txt;
    }

    public String getTim_bg() {
        return tim_bg;
    }

    public void setTim_bg(String tim_bg) {
        this.tim_bg = tim_bg;
    }

    public String getLogo_txt() {
        return logo_txt;
    }

    public void setLogo_txt(String logo_txt) {
        this.logo_txt = logo_txt;
    }

    public String getLogo_bg() {
        return logo_bg;
    }

    public void setLogo_bg(String logo_bg) {
        this.logo_bg = logo_bg;
    }

    
}
