package datamanager.config;
import classmanager.config.ConfigCategoryMgr;
import classmanager.config.ConfigMgr;
import classmanager.config.ConfigSchemeMgr;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import modules.Home;
import modules.configuration.CategoryManagement;
import modules.configuration.ColorDetails;
import modules.configuration.FontDetails;
import modules.configuration.ProductConfiguration;

public class Config {

    //database variables
    public static Connection conn = null;
    public static PreparedStatement pstmt = null;
    public static Statement stmt = null;
    public static ResultSet rs = null;
    public static String sql = null;    
    
    //product variables
    //class managers
    public static ConfigMgr configmgr = null;
    public static ConfigColorDetails config_colordetails = null;
    public static FontSetting fontsetting = null;
    public static ArrayList<ConfigScheme> configscheme = null;
    public static ArrayList<ConfigCategory> configcategory = null;
    public static ConfigSchemeMgr configschememgr= null;
    public static ConfigCategoryMgr configcategorymgr= null;
    public static ProductConfiguration productconfiguration= null;
    public static CategoryManagement categorymanagement= null;
    public static Home home = null;    
    public static ColorDetails colordetails = null;    
    public static FontDetails fontdetails = null;    
    public static boolean check = false;
    
}
