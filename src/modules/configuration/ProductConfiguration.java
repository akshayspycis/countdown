package modules.configuration;

import datamanager.config.Config;
import datamanager.config.ConfigScheme;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;


public class ProductConfiguration extends javax.swing.JDialog {
    
    DefaultTableModel tbl_schememodel;
    public ProductConfiguration(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
//        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icon.png")));
        
        tbl_schememodel = (DefaultTableModel) tbl_Scheme.getModel();
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });        
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        btn_SchemeAdd = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        txt_section = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txt_min = new javax.swing.JSpinner();
        jLabel1 = new javax.swing.JLabel();
        cb_category = new javax.swing.JComboBox();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_Scheme = new javax.swing.JTable();
        btn_SchemeRemove = new javax.swing.JButton();
        btn_SchemeSave = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Section Configuration");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));

        btn_SchemeAdd.setText("Add");
        btn_SchemeAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SchemeAddActionPerformed(evt);
            }
        });

        jLabel9.setText("Time (Min.)");

        jLabel8.setText("Section Name");

        jLabel1.setText("Category");

        cb_category.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_categoryActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cb_category, 0, 100, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_section, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_min, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(btn_SchemeAdd)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(txt_min, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_section, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_SchemeAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)
                        .addComponent(cb_category, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel9Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_SchemeAdd, jLabel8, jLabel9});

        tbl_Scheme.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_Scheme.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Section", "Time"
            }
        ));
        tbl_Scheme.setRowHeight(20);
        tbl_Scheme.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_Scheme.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(tbl_Scheme);

        btn_SchemeRemove.setText("Remove");
        btn_SchemeRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SchemeRemoveActionPerformed(evt);
            }
        });

        btn_SchemeSave.setText("Save");
        btn_SchemeSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SchemeSaveActionPerformed(evt);
            }
        });

        jButton1.setText("Start Time");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Stop All");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(btn_SchemeRemove)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_SchemeSave))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_SchemeRemove, btn_SchemeSave});

        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_SchemeRemove)
                    .addComponent(btn_SchemeSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //checked
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    //checked
    private void btn_SchemeSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SchemeSaveActionPerformed
        ConfigScheme[] scheme = new ConfigScheme[tbl_schememodel.getRowCount()];
        int i;
        try {
            for (i = 0; i < scheme.length; i++) {
                scheme[i] = new ConfigScheme();
                scheme[i].setCategory_id(Config.configcategory.get(cb_category.getSelectedIndex()).getCategory_id());
                scheme[i].setAmount(tbl_Scheme.getValueAt(i, 0).toString());
                scheme[i].setPercentage(tbl_Scheme.getValueAt(i, 1).toString());
            }
            if (Config.configschememgr.insScheme(scheme,Config.configcategory.get(cb_category.getSelectedIndex()).getCategory_id())) {
                //onloadReset();
                JOptionPane.showMessageDialog(this,"Section configure successfully.","Configure Successful.",JOptionPane.NO_OPTION);
            } else {
                JOptionPane.showMessageDialog(this,"Problem in configure process.","Error",JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_SchemeSaveActionPerformed

    //checked
    private void btn_SchemeRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SchemeRemoveActionPerformed
        try {
            int srs[] = tbl_Scheme.getSelectedRows();
            for (int i = 0; i < srs.length; i++) {
                tbl_schememodel.removeRow(srs[i]-i);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select any row from the table.", "No row selected.", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_SchemeRemoveActionPerformed

    //checked
    private void btn_SchemeAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SchemeAddActionPerformed
        String section = txt_section.getText().toUpperCase();
        int value = (Integer) txt_min.getValue();

        if(!section.equals("") && (value!=0 &&value > 0)) {
            int i;
            for (i = 0; i < tbl_schememodel.getRowCount(); i++) {
                if (tbl_Scheme.getValueAt(i, 0).equals(section) && tbl_Scheme.getValueAt(i, 1).equals(value)) {
                    break;
                }
            }
            if (i == tbl_schememodel.getRowCount()) {
                tbl_schememodel.addRow(new Object[] {section,value});
                txt_section.setText("");
                txt_min.setValue(0);
                txt_section.requestFocus();
            } else {
                JOptionPane.showMessageDialog(this, "Entry already exits.", "Multiple entry", JOptionPane.ERROR_MESSAGE);
                txt_section.requestFocus();
            }
        } else {
            JOptionPane.showMessageDialog(this, "All fields are mandatory or doesn't less or equal zero.", "Error", JOptionPane.ERROR_MESSAGE);
            txt_section.requestFocus();
        }
    }//GEN-LAST:event_btn_SchemeAddActionPerformed

    private void cb_categoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_categoryActionPerformed
        tbl_schememodel.setRowCount(0);
            try {
                for (int i = 0; i < Config.configscheme.size(); i++) {
                    if(Config.configscheme.get(i).getCategory_id().equals(Config.configcategory.get(cb_category.getSelectedIndex()).getCategory_id())){
                        tbl_schememodel.addRow(new Object[] { Config.configscheme.get(i).getAmount(), Config.configscheme.get(i).getPercentage() });
                    }
                }
                txt_section.setText(null);
            } catch (Exception e) {}
    }//GEN-LAST:event_cb_categoryActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        dispose();
        Config.home.startCountdown_Action(Config.configcategory.get(cb_category.getSelectedIndex()).getCategory_id());
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Config.home.stopAll_Action();
    }//GEN-LAST:event_jButton2ActionPerformed
        
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_SchemeAdd;
    private javax.swing.JButton btn_SchemeRemove;
    private javax.swing.JButton btn_SchemeSave;
    private javax.swing.JComboBox cb_category;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable tbl_Scheme;
    private javax.swing.JSpinner txt_min;
    private javax.swing.JTextField txt_section;
    // End of variables declaration//GEN-END:variables
   
    //checked
    public void onloadReset() {        
        cb_category.removeAllItems();
        for (int i = 0; i < Config.configcategory.size(); i++) {
            cb_category.addItem(Config.configcategory.get(i).getCategory());
        }
        cb_categoryActionPerformed(null);
    }
    
}