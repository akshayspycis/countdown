/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package timer;

import classmanager.config.ConfigMgr;
import datamanager.config.Config;
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Akshay
 */
public class Timer {

    static banner banner;
    
    public Timer() {
        banner = new banner();
        Config.configmgr = new ConfigMgr();
    }    
    
    private void showbanner() {        
        banner.setVisible(true);
        banner.setBannerLabel("Checking system configuration...");
    }
    
    private void connserver() {
        banner.setBannerLabel("Connecting to server...");        
    }
    
    private void conndatabase() {
        banner.setBannerLabel("Checking databse connection...");
//        if (!Config.configmgr.loadDatabase()) {
//            JOptionPane.showMessageDialog(banner,"Database connectivity problem, product can not be launch.","Error : 10068.",JOptionPane.ERROR_MESSAGE);            
//            System.exit(0);
//        }
    }
    
    private void initcomponent() {
        banner.setBannerLabel("Initialising components...");        
        int x=0;
        if (Config.configmgr.loadSetting()){x++;} else {System.out.println("Error : 1, Setting loading problem.");}
        
        if (Config.configmgr.loadFontSetting()){x++;} else {System.out.println("Error : 2, Font loading problem.");}
        if (Config.configmgr.loadScheme()){x++;} else {System.out.println("Error : 4, Section loading problem.");}
        if (Config.configmgr.loadCategory()){x++;} else {System.out.println("Error : 3, Category loading problem.");}
        if (Config.configmgr.loadClassManager()){x++;} else {System.out.println("Error : 5, ClassManager loading problem.");}
        if (Config.configmgr.loadForms()){x++;} else {System.out.println("Error : 6, Forms loading problem.");}
        if (x!=6) {
            JOptionPane.showMessageDialog(banner,"Product initialisation problem, product can not be launch."+x,"Error.",JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }
    
    private void hidebanner() {
        banner.dispose();
        Config.home.onloadReset();
        Config.home.setVisible(true);
    }
    
    public static void main(String[] args) {
        try {        
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Timer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(Timer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Timer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Timer.class.getName()).log(Level.SEVERE, null, ex);
        }               
        
        final Timer obj = new Timer();
        Thread t1 = new Thread() {
           public void run() {
                   loadAutoData();    
            }
           private void loadAutoData() {
                       try {            
            
            obj.showbanner();        
//            Thread.sleep(1000);
            //obj.connserver();
  //          Thread.sleep(1000);
            //obj.conndatabase();
            Thread.sleep(1000);
            obj.initcomponent();
//            Thread.sleep(10000);
            obj.hidebanner();            
        } catch (InterruptedException ex) {
            Logger.getLogger(Timer.class.getName()).log(Level.SEVERE, null, ex);
        } 

           }
        };

        t1.start();
      

    }
}
