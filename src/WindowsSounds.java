import java.awt.Toolkit;

public class WindowsSounds {
  public static void main(String ... args) throws InterruptedException {
    System.out.println("Sound 1");
    Runnable sound1 =
      (Runnable)Toolkit.getDefaultToolkit().getDesktopProperty("win.sound.hand");
    if(sound1 != null) sound1.run();
    Thread.sleep(1000);
    System.out.println("Sound 2");
    Runnable sound2 =
      (Runnable)Toolkit.getDefaultToolkit().getDesktopProperty("win.sound.question");
    if(sound2 != null) sound2.run();

    System.out.println("Supported windows property names:");
    String propnames[] = (String[])Toolkit.getDefaultToolkit().getDesktopProperty("win.propNames");
    for(int i = 0; i < propnames.length; i++) {
      if (propnames[i].startsWith("win.sound.")) {
        System.out.println(propnames[i]);
      }
    }
  }
}